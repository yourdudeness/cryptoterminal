import 'jquery.global.js';
import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import {
    disableBodyScroll,
    enableBodyScroll,
    clearAllBodyScrollLocks
} from 'body-scroll-lock';

import AOS from 'aos';




import Swiper from 'swiper/bundle';



import mainPage from 'index.page';
import autoComplete from "@tarekraafat/autocomplete.js";
import './libs/ui.js';
import './libs/mousewheel.js';
import './libs/kinetic.js';
import './libs/divScroll';

const jQuery = require("jquery");
window.jQuery = window.$ = jQuery; // This only needs to be imported once in your app
jQuery.fn.load = function (callback) { $(window).on("load", callback) };


let app = {


    scrollToOffset: 200, // оффсет при скролле до элемента
    scrollToSpeed: 500, // скорость скролла

    init: function () {
        // read config
        if (typeof appConfig === 'object') {
            Object.keys(appConfig).forEach(key => {
                if (Object.prototype.hasOwnProperty.call(app, key)) {
                    app[key] = appConfig[key];
                }
            });
        }

        app.currentID = 0;

        // Init page
        this.page = page;
        this.page.init.call(this);

        this.forms = forms;
        this.forms.init.call(this);

        // Init page

        this.mainPage = mainPage;
        this.mainPage.init.call(this);

        //window.jQuery = $;
        window.app = app;

        app.document.ready(() => {

            AOS.init();

            $("#makeMeScrollable").smoothDivScroll({

                touchScrolling: true,
                autoScrollingMode: "onStart",
                mousewheelScrolling: "allDirections",
            });

            const elem = document.getElementById('charts');



            let header = document.querySelector('.main-header');
            window.addEventListener('scroll', function () {


                if (pageYOffset > 50) {
                    header.classList.add('active');
                } else if (pageYOffset == 0) {
                    header.classList.remove('active');
                }
            });

            let swiper2 = new Swiper('.swiper', {
                direction: 'vertical',
                slidesPerView: 1,
                spaceBetween: 30,
                mousewheel: true,
                pagination: {

                    clickable: true,
                },

                on: {
                    slideChange: function () {
                        document.addEventListener('scroll', function () {
                            const posTop = elem.getBoundingClientRect().top;

                            // Блок достиг верхней границы экрана (или выше)


                            if (posTop <= 108) {
                                // disableBodyScroll(elem);
                            }

                            // Блок только появляется снизу (или выше)
                            // elem.classList.toggle('visible', posTop < window.innerHeight);

                            // Блок целиком находится в видимой зоне
                            // elem.classList.toggle('visible', posTop + elem.clientHeight <= window.innerHeight && posTop >= 0);
                        });
                        if (swiper2.activeIndex == 1) {
                            clearAllBodyScrollLocks();
                        }
                    },
                },
            });
            //Scroll Gsap

        });




        app.window.on('load', () => {


            let swiper = new Swiper(".delta-slider", {
                pagination: {
                    el: ".swiper-pagination",
                    type: "fraction",
                    slidesPerView: 1,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
            });







            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });
        });

        // this.document.on(app.resizeEventName, () => {
        // });

    },

};
app.init();
